﻿#pragma strict
var speed=50;
var explosion : Transform;
var sound : AudioClip;
var sound1 : AudioClip;

function Start () {
	transform.Rotate(new Vector3(0,0,-90),Space.Self);
}

function Update () {
	var amtToMove = speed* Time.deltaTime;
	transform.Translate(Vector3.up*amtToMove);
	
	if(transform.position.x > 37)
		Destroy(gameObject);
}